﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroScript : MonoBehaviour {

    public enum State
    {
        Idle = 0, Moving = 1, Attacking = 2, InventoryOpen = 3        
    };

    public State m_state;
    public float m_speed;
    public GameManager gm;
    public Vector3 m_facingDirection;
    private int damageHeroDeals; // TODO: very rudimentary.  will be based off of weapons/combat system.

    void Start () {
        damageHeroDeals = 1;
    }

    public void SetAttackState()
    {
        m_state = State.Attacking;
        StartCoroutine(AttackingState());
    }

    private IEnumerator AttackingState()
    {
        yield return new WaitForSeconds(0.25f);
        m_state = State.Idle;
    }
    
    void Update() {
        if(m_state == State.Moving)
            m_state = State.Idle;

        float distance = m_speed * Time.deltaTime;
        BoardManager bm = gm.GetBoardManager;

        int heroGridX = Mathf.FloorToInt(transform.position.x);
        int heroGridY = Mathf.FloorToInt(transform.position.y);

        if (m_state != State.Attacking)
        {

            //lr.SetPosition(0, new Vector3(heroGridX - 1, heroGridY, -0.1f));
            //lr.SetPosition(1, new Vector3(heroGridX - 1, heroGridY + 1, -0.1f));
            //lr.SetPosition(2, new Vector3(heroGridX, heroGridY + 1, -0.1f));
            //lr.SetPosition(3, new Vector3(heroGridX, heroGridY, -0.1f));
            //lr.SetPosition(4, new Vector3(heroGridX - 1, heroGridY, -0.1f));
            //LineRenderer lr = this.GetComponent<LineRenderer>();
            string animationToPlay = null;
            Animator heroAnim = GetComponent<Animator>();
            bool goingUp = Input.GetKey("w");
            bool goingDown = Input.GetKey("s");
            bool goingLeft = Input.GetKey("a");
            bool goingRight = Input.GetKey("d");
            BoardTile testedTile = null;

            if (goingLeft)
            {
                if (bm.IsGridTraversible(heroGridX, heroGridY, out testedTile))
                {
                    m_state = State.Moving;
                    m_facingDirection = Vector3.left;
                    this.transform.position += m_facingDirection * distance;
                    BoardTile ignoreThisBadDesign = null;
                    if (!bm.IsGridTraversible(Mathf.FloorToInt(transform.position.x), heroGridY, out ignoreThisBadDesign))
                    {
                        transform.position = new Vector3(
                            testedTile.m_boardTile.transform.position.x,
                            transform.position.y, 0.0f);
                    }
                    animationToPlay = "HeroWalkWest";
                }
            }
            if (goingRight)
            {
                if (bm.IsGridTraversible(heroGridX+1, heroGridY, out testedTile))
                {
                    m_state = State.Moving;
                    m_facingDirection = Vector3.right;
                    this.transform.position += m_facingDirection * distance;
                    animationToPlay = "HeroWalkEast";
                }
            }
            if (goingDown)
            {
                if (bm.IsGridTraversible(heroGridX, heroGridY, out testedTile))
                {
                    m_state = State.Moving;
                    m_facingDirection = Vector3.down;
                    this.transform.position += m_facingDirection * distance;
                    BoardTile ignoreThisBadDesign = null;
                    if (!bm.IsGridTraversible(heroGridX, Mathf.FloorToInt(transform.position.y), out ignoreThisBadDesign))
                    {
                        transform.position = new Vector3(
                            transform.position.x,
                            testedTile.m_boardTile.transform.position.y, 0.0f);
                    }
                    animationToPlay = "HeroWalkSouth";
                }
            }
            if (goingUp)
            {
                if (bm.IsGridTraversible(heroGridX, heroGridY+1, out testedTile))
                {
                    m_state = State.Moving;
                    m_facingDirection = Vector3.up;
                    this.transform.position += m_facingDirection * distance;
                    animationToPlay = "HeroWalkNorth";
                }
            }
            if (m_state == State.Moving)
            {
                heroAnim.Play(animationToPlay);
            }
        }
        else // attacking
        {
            List<GameObject> deadEnemies = new List<GameObject>();
            float attackRadius = 1.0f;
            foreach (GameObject enemy in gm.GetComponentInParent<GameManager>().m_enemies)
            {
                Vector3 heroToEnemy = enemy.transform.position - this.transform.position;
                float magnitude = heroToEnemy.magnitude;
                heroToEnemy.Normalize();
                if (magnitude <= attackRadius)
                {
                    float dotProduct = Vector3.Dot(m_facingDirection, heroToEnemy);
                    if (dotProduct >= 0) // eliminates enemies behind us
                    {
                        BadGuyMelee bgm = enemy.GetComponent<BadGuyMelee>();
                        bool dead = bgm.receivesDamage(damageHeroDeals);
                        bgm.applyHit(m_facingDirection);
                        if(dead)
                            deadEnemies.Add(enemy);
                        // if we want to attack *one* enemy in front of us.... do the following:
                        /*
                         *  Take the cross product of the same two vectors. 
                         *  Only need the z value, so just use a.x*b.y - a.y*b.x. 
                         *  If this enemy has a higher (or lower, depending on handedness of the system) 
                         *    than your current marked enemy, 
                         *    then it becomes the new marked enemy (I.e, the one that will be attacked).
                         *    Repeat until all enemies are processed. 
                         *    You will now have a marked enemy, 
                         *    which represents the first enemy in front of the player that gets hit by the swinging weapon
                         */
                    }
                }
            }

            foreach (GameObject enemy in deadEnemies)
            {
                gm.GetComponentInParent<GameManager>().m_enemies.Remove(enemy);
                Object.Destroy(enemy);
            }
        }
    }
}
