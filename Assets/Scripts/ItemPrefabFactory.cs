﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ItemFactory
{

    public static Dictionary<ItemPrefabType, string> s_TypeToPrefabName = new Dictionary<ItemPrefabType, string>()
    {
        { ItemPrefabType.Branch, "branchPrefab" },
        { ItemPrefabType.Bread, "breadPrefab" },
        { ItemPrefabType.Canteen, "CanteenPrefab" },
        { ItemPrefabType.Cloth, "ClothPrefab" },
        { ItemPrefabType.Coal, "CoalPrefab" },
        { ItemPrefabType.Crab, "crabPrefab" },
        { ItemPrefabType.Melon, "MelonPrefab" },
        { ItemPrefabType.Mud, "MudPrefab" },
        { ItemPrefabType.Pickaxe, "pickaxe2Prefab" },
        { ItemPrefabType.Rock, "RockPrefab" },
        { ItemPrefabType.Root, "RootPrefab" },
        { ItemPrefabType.Salt, "SaltPrefab" },
        { ItemPrefabType.Shovel, "ShovelPrefab" },
        { ItemPrefabType.StoneCudgel, "StoneCudgelPrefab" },
        { ItemPrefabType.Torch, "torchPrefab" },
        { ItemPrefabType.Water, "waterPrefab" },
    };
    public static Dictionary<ItemPrefabType, string> s_TypeToName = new Dictionary<ItemPrefabType, string>()
    {
        { ItemPrefabType.Branch, "branch" },
        { ItemPrefabType.Bread, "bread" },
        { ItemPrefabType.Crab, "crab" },
        { ItemPrefabType.Water, "water" },
        { ItemPrefabType.Canteen, "canteen" },
        { ItemPrefabType.Cloth, "cloth" },
        { ItemPrefabType.Coal, "coal" },
        { ItemPrefabType.Melon, "melon" },
        { ItemPrefabType.Mud, "mud" },
        { ItemPrefabType.Pickaxe, "pickaxe2" },
        { ItemPrefabType.Rock, "rock" },
        { ItemPrefabType.Root, "root" },
        { ItemPrefabType.Salt, "salt" },
        { ItemPrefabType.Shovel, "shovel" },
        { ItemPrefabType.StoneCudgel, "stoneCudgel" },
        { ItemPrefabType.Torch, "torch" },
    };
    private static Dictionary<ItemPrefabType, List<GameObject>> s_ItemPrefabs = 
        new Dictionary<ItemPrefabType, List<GameObject>>();


    public static GameObject CreateItemPrefab(ItemPrefabType itemType)
    {
        Debug.LogFormat("Instantiating object {0}", itemType.ToString());
        Debug.AssertFormat(s_TypeToPrefabName.ContainsKey(itemType), "Missing Dictionary Entry for {0}", itemType);

        GameObject obj = Object.Instantiate(
            Resources.Load(s_TypeToPrefabName[itemType]) as GameObject,
            position: new Vector3(0, 0, 0),
            rotation: Quaternion.identity);
        {
            obj.GetComponent<SpriteRenderer>().enabled = false;
            Image img = obj.GetComponent<Image>();
            img.transform.name = s_TypeToName[itemType];
            img.sprite = Resources.Load<Sprite>(s_TypeToName[itemType]);
        }
        if (!s_ItemPrefabs.ContainsKey(itemType))
            s_ItemPrefabs.Add(itemType, new List<GameObject>());
        s_ItemPrefabs[itemType].Add(obj);
        return obj;
    }
    public static Item CreateItem(ItemPrefabType type)
    {
        switch (type)
        {
            case ItemPrefabType.Branch:
            case ItemPrefabType.Bread:
            case ItemPrefabType.Canteen:
            case ItemPrefabType.Cloth:
            case ItemPrefabType.Coal:
            case ItemPrefabType.Crab:
            case ItemPrefabType.Melon:
            case ItemPrefabType.Mud:
            case ItemPrefabType.Pickaxe:
            case ItemPrefabType.Rock:
            case ItemPrefabType.Root:
            case ItemPrefabType.Salt:
            case ItemPrefabType.Shovel:
            case ItemPrefabType.StoneCudgel:
            case ItemPrefabType.Torch:
            case ItemPrefabType.Water:
                return new Item(type, ItemFactory.CreateItemPrefab(type));
            default: Debug.AssertFormat(false, "Cannot create item of unknown type: {0}", type.ToString()); return null;

        }
    }
}
