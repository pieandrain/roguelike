﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : Item {

    public float x, y;
    public bool movesWithHero;
    public LightSize m_size;
     

    public enum LightSize
    {
        Small = 0,
        Medium = 1,
        Large = 2
    };
    public Torch(float _x, float _y, ItemPrefabType type, GameObject obj, LightSize size, bool _movesWithHero) : base(_x, _y, type, obj)
    {
        
        x = _x; y = _y; movesWithHero = _movesWithHero;
        m_size = size;
    }

    public Torch(ItemPrefabType type, GameObject obj, LightSize size, bool _movesWithHero) : base(type, obj)
    {

        x = 0; y = 0; movesWithHero = _movesWithHero;
        m_size = size;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
