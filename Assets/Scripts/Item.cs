﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ItemPrefabType
{
    Branch, Bread, Canteen, Cloth, Coal, Crab, Melon, Mud, Pickaxe, Rock, Root, Salt, Shovel, StoneCudgel, Torch, Water,
};


public class Item
{
    private void Init(float x, float y, ItemPrefabType type, GameObject obj)
    {
        m_item = obj;
        m_stat = "none";
        m_effect = 0;
        m_x = x;
        m_y = y; ;
        m_type = type;
        m_halfLife = 10;
        m_movesWithPlayer = false;
        m_spriteRender = obj.GetComponent<SpriteRenderer>() != null ? obj.GetComponent<SpriteRenderer>() : null;
        m_image = obj.GetComponent<Image>() != null ? obj.GetComponent<Image>() : null;
    }

    public Item(float x, float y, ItemPrefabType type, GameObject obj)

    {
        Init(x, y, type, obj);
    }

    public Item(ItemPrefabType type, GameObject obj)
    {
        Init(0, 0, type, obj);
    }

    public float m_x;
    public float m_y;
    //public LightSize m_size;//torch use
    public bool m_movesWithPlayer;//torch use
    public string m_name;//obj instance name(torch1, canteen2, etc)
    public string m_stat;
    public int m_effect;
    public ItemPrefabType m_type;
    public float m_halfLife;
    public GameObject m_item;
    public Image m_image = null;
    public SpriteRenderer m_spriteRender = null;
  
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
