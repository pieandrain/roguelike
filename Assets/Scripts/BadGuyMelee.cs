﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadGuyMelee : MonoBehaviour {
    public float speed;
    public GameObject hero;
    public int m_hitpoints;
    public GameManager gm;

    enum State
    {
        Idle, Aggro
    };

    void Start () {
        m_state = State.Idle;
        m_hitpoints = 3;
	}

    private float m_aggroDistance = 4;
    private State m_state;
	void Update ()
    {
        float badguyX = this.transform.position.x;
        float badguyY = this.transform.position.y;
        if (m_state == State.Idle)
        {
            float dist = Vector3.Distance(this.transform.position, hero.transform.position);
            if (dist <= m_aggroDistance)
                m_state = State.Aggro;
        }
        else if (m_state == State.Aggro)
        {
            float distance = speed * Time.deltaTime;

            Vector3 direction = hero.transform.position - this.transform.position;
            direction.Normalize();


            Vector3 newPosition = this.transform.position + (direction * distance);

            BoardManager bm = gm.GetBoardManager;

            // if this would jam the monster into the wall, try to move around it (TODO: we may want to just go to A* to aovid this)
            BoardTile tile = null;
            if (!bm.IsGridTraversible((int)newPosition.x, (int)(newPosition.y), out tile)) 
            {
                // is x-axis a problem?
                if ((int)badguyX != (int)newPosition.x)
                {
                    newPosition.x = badguyX; // ignore the x-axis portion of the move and move a bit more along y
                    newPosition.y *= 1.001f;
                }
                // is y-axis a problem?
                if ((int)badguyY != (int)newPosition.y)
                {
                    newPosition.y = badguyY; // ignore the y-axis portion of the move and move a bit more along x
                    newPosition.x *= 1.001f;
                }
            }
            this.transform.position = newPosition;
        }
    }

    public void applyHit(Vector3 forceUnitVector /*heros facing*/)
    {
        this.transform.position += forceUnitVector * 1.0f;
    }

    public bool receivesDamage(int damage)
    {
        m_hitpoints -= damage;
        return m_hitpoints <= 0;
    }
}
