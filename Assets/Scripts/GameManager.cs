﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;



//TODO adjust dropItem to delete y and x when needed
public class GameManager : MonoBehaviour
{
    public GameObject m_hero;    //prefab (and we use it for the gameobject at runtime)
    public GameObject m_badguyPrefab; //prefab
    private GameObject m_torchPrefab;
    public GameObject m_darknessMaskPrefab;
    private BoardManager m_boardManager;
    public BoardManager GetBoardManager
    {
        get {
            if (m_boardManager == null)
            {
                m_boardManager = this.GetComponent<BoardManager>();
            }
            return m_boardManager;
        }
    }

    public GameObject m_camera;
    private LightingSystem m_lightingSystem = null; // set at start
    

    public List<GameObject> m_enemies;
    public GameObject m_itemPicked;
    public InventoryScript inventory;
    //public StatScript stats;
    private CraftingScript m_craftingMenu;
    public void SetCraftingMenu(CraftingScript script)
    {
        m_craftingMenu = script;
    }
        
    Dictionary<int, Dictionary<int, Item>> m_torchDic = new Dictionary<int, Dictionary<int, Item>>();
    public static List<Item> m_itemsInWorld = new List<Item>();
    
    public static Dictionary<string, Dictionary<string, int>> m_itemStatsDic = new Dictionary<string, Dictionary<string, int>>();//stats effect for items

    public static List<GameObject> s_wallTiles = new List<GameObject>();
    private GameObject m_fpsTextField;
    private GameObject m_controlMenuLeft;
    private bool m_controlMenuOpen = false;
    List<Item> m_torchesInWorld;

    private void OnGUI()
    {
        Event e = Event.current;
        if (e.isKey && e.type == EventType.KeyUp)
        {
            switch (e.keyCode)
            {
                case KeyCode.Space:
                    m_hero.GetComponent<HeroScript>().SetAttackState();
                    break;
            }
        }        
    }
    
    void Start()
    {
        m_fpsTextField = GameObject.Find("FPS_Text");
        m_torchesInWorld = new List<Item>();

        inventory = this.GetComponent<InventoryScript>();
        //inventory.InitInventoryList();
        //m_controlMenuLeft = GameObject.Find("controlMenuPrefabLeft");
        //m_controlMenuLeft.SetActive(false);

        int heroPosX = 3; int heroPosY = 3;
        GameObject go1 = Instantiate(m_hero) as GameObject;
        go1.transform.position = new Vector3(heroPosX, heroPosY, 0);
        go1.name = "HERO";
        m_hero = go1;
        SpriteRenderer sr = go1.GetComponent<SpriteRenderer>();
        sr.sortingLayerName = "HERO";
        float llx = heroPosX -0.5f;
        float lly = heroPosY -0.5f;
        float urx = heroPosX +0.5f;
        float ury = heroPosY +0.5f;

        LineRenderer lr = m_hero.AddComponent<LineRenderer>();
        {

#if UNITY_EDITOR
            lr.material = new Material(Shader.Find("Particles/Additive"));
#endif

            lr.widthMultiplier = 0.1f;
            lr.positionCount = 5;
            lr.startColor = new Color(0, 0, 1, 1);
            lr.endColor = new Color(0, 0, 1, 1);
            lr.SetPosition(0, new Vector3(llx, lly, -0.1f));
            lr.SetPosition(1, new Vector3(llx, ury, -0.1f));
            lr.SetPosition(2, new Vector3(urx, ury, -0.1f));
            lr.SetPosition(3, new Vector3(urx, lly, -0.1f));
            lr.SetPosition(4, new Vector3(llx, lly, -0.1f));
            lr.sortingLayerName = "DARKNESS_MASK";
        }

        go1.GetComponent<SpriteRenderer>().enabled = true;

        HeroScript hs1 = go1.GetComponent<HeroScript>();
        hs1.gm = this;
        hs1.m_speed = 4;
      
        // lighting system
        {            
            m_lightingSystem = GetComponent<LightingSystem>();
            m_lightingSystem.setHero(m_hero);
            m_lightingSystem.setGameManager(gameObject);
            m_lightingSystem.m_defaultOpacity = 0.85f;
            m_lightingSystem.m_deepDarkOpacity = 0.95f;            
        }

        inventory.InitHotbarList();
        
        //stats = GetComponent<StatScript>();
        //stats.InitStats();
        //coroutine = stats.DepleteStats();
        //StartCoroutine(coroutine);
        GetBoardManager.CreateDungeon();

        inventory.UpdateInventory();
    }

    public void OnDestroy()
    {
        GetBoardManager.DeleteTiles();
    }

    public float m_avgFrameRate;
    private void LateUpdate()
    {
        RectTransform trans = m_hero.GetComponent<RectTransform>();
        Vector3[] corners = new Vector3[4];
        trans.GetWorldCorners(corners);
        Vector3 bottomLeft = corners[0];
        Vector3 topRight = corners[2];
        float halfWidth = (topRight.x - bottomLeft.x) * 0.5f;
        float halfHeight = (topRight.y - bottomLeft.y) * 0.5f;

        m_camera.transform.position = new Vector3(bottomLeft.x + halfWidth, bottomLeft.y + halfHeight, m_camera.transform.position.z);
#if UNITY_EDITOR
        m_lightingSystem.UpdateDarknessMaskPosition(
        bottomLeft.x + halfWidth, 
        bottomLeft.y + halfHeight);
#endif
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();   
        }
        m_avgFrameRate = Time.frameCount / Time.time;
        if (m_fpsTextField != null)
        {
            Text textField = m_fpsTextField.GetComponent<Text>();
            if (textField != null)
                textField.text = m_avgFrameRate.ToString();
        }

        float x = m_hero.transform.position.x;
        float y = m_hero.transform.position.y;

        if (Input.GetKeyDown("i"))
        {
            if (!m_controlMenuOpen)
            {
                //m_controlMenuLeft.SetActive(true);
                m_controlMenuOpen = true;
                InitControlsMenu();
            }
            else
            {
                //m_controlMenuLeft.SetActive(false);
                m_controlMenuOpen = false;
            }
        }

        //place torch
        if (Input.GetKeyDown("t"))
        {
            PlaceTorch(x, y);
        }

        //equip/use item
        if (Input.GetKeyDown(KeyCode.UpArrow))
            if (!m_craftingMenu.craftMenuOpen)
                UseItem();

        if (Input.GetKeyDown(KeyCode.DownArrow))
            if (!m_craftingMenu.craftMenuOpen)
                DropItem(m_hero.transform.position.x, m_hero.transform.position.y);

        // pickup item
        if (Input.GetKeyDown("y"))
        {
            Item item = ItemToPickUp();
            if (item != null)
            {             
                RemoveItemFromWorld(item);
                inventory.AddHotbarItem(item);
                inventory.UpdateInventory();
            }
        }

        List<LightingSystem.Torch> torches =
            m_torchesInWorld.Select(item => new LightingSystem.Torch(item.m_x, item.m_y, false)).ToList();
        torches.Add(new LightingSystem.Torch(m_hero.transform.position.x, m_hero.transform.position.y, true));
#if UNITY_EDITOR
        m_lightingSystem.ApplyLights(torches);
#endif
        if (Input.GetKeyDown("c"))
        {
            if (m_craftingMenu.craftMenuOpen)
                m_craftingMenu.HideCraftingMenu();
            else
                m_craftingMenu.ShowCraftingMenu();
        }

        
    }
 
    void InitControlsMenu()
    {
        //Text txt = m_controlMenuLeft.GetComponent<Text>();
        //txt.text = 
        //@"Controls:
        //    esc:  kill the app
        //    ti:    open/close control menu
        //    q:    open/close inventory
        //    t:    place torch
        //    y:    pickup item
        //    c:    open/shut crafting menu
        //    r:    craft selected item in craft menu
        //    downArrow:     drop selected inventory item
        //    leftArrow:     navigate inventory
        //    rightArrow:    navigate inventory";
    }
    
    // returns null if no items in proximity
    private Item ItemToPickUp()
    {
        foreach (Item item in m_itemsInWorld)
            if (Collides(item.m_item, m_hero))
                return item;

        return null;
    }

    void RemoveItemFromWorld(Item item)
    { 
        item.m_item.GetComponent<SpriteRenderer>().enabled = false;
        m_itemsInWorld.Remove(item);
    }

    public void PlaceItemInWorld(float x, float y, Item item)
    {
        Debug.Assert(item != null, "Item can't be null");
        SpriteRenderer objRend = item.m_item.GetComponent<SpriteRenderer>();
        objRend.enabled = true;

        RectTransform rt = item.m_item.GetComponent<RectTransform>();
        Debug.Assert(rt != null, "Item has no RectTransform");
        rt.localPosition = new Vector3(x, y, 0);
        rt.sizeDelta = new Vector2(1, 0);

        m_itemsInWorld.Add(item);
    }

    private void DropItem(float x, float y)
    {
        Item droppedItem = inventory.GetAndRemoveSelectedHotbarItem();
        if (droppedItem != null)
            PlaceItemInWorld(x, y, droppedItem);
    }

    private void PlaceTorch(float x, float y)
    {
        Item torch = inventory.GetAndRemoveItem(ItemPrefabType.Torch);
        if (torch != null)
            PlaceItemInWorld(x, y, torch);
    }

    public void UseItem()
    {
       /* Item useItem;

        useItem = inventory.GetSelectedHotbarItem();
        ItemType type = useItem.m_type;
        StatScript.ItemUpdateStat(useItem.m_name, type);            

        if (useItem.m_type == ItemType.Torch)
        {
            m_torchesInWorld.Remove(useItem);
        }
        m_itemsInWorld.Remove(useItem);
        inventory.UpdateInventory();*/
    }


    bool Collides(GameObject obj1, GameObject obj2)
    {
        RectTransform obj1Trans = obj1.GetComponent<RectTransform>();
        Vector3[] obj1Corners = new Vector3[4];
        obj1Trans.GetWorldCorners(obj1Corners);
        Vector3 obj1BottomLeft = obj1Corners[0];
        Vector3 obj1TopRight = obj1Corners[2];

        RectTransform obj2Trans = obj2.GetComponent<RectTransform>();
        Vector3[] obj2Corners = new Vector3[4];
        obj2Trans.GetWorldCorners(obj2Corners);
        Vector3 obj2BottomLeft = obj2Corners[0];
        Vector3 obj2TopRight = obj2Corners[2];

        if (obj2BottomLeft.x > obj1TopRight.x ||
            obj2BottomLeft.y > obj1TopRight.y ||
            obj2TopRight.x < obj1BottomLeft.x ||
            obj2TopRight.y < obj1BottomLeft.y)
        {
            return false;
        }
        return true;
    }
}
