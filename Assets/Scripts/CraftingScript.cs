﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//TODO test CraftItem: bug: it will create infinte number of torches without the needed crafting material items

public class CraftingScript : MonoBehaviour {

    public bool craftMenuOpen = false;

    private int minPaddingTop = 15;
    public GameObject craftingOptionsPanel = null;
    public GameObject requirementsPanel = null;

    // prefabs for specific item containers
    public GameObject optionPrefab = null;
    public GameObject requirementPrefab = null;

    // overlays the selected item 
    public GameObject bloodFramePrefab = null;

    // instantiation of 'bloodFramePrefab'
    private GameObject bloodFrame = null;

    private int selectedIndex = 0;
    private GameManager gm;

    private class CraftingDisplay
    {
        public GameObject itemObject = null;
        public ItemPrefabType type;
        public CraftingDisplay(ItemPrefabType option)
        {
            type = option;
            GameObject obj = ItemFactory.CreateItemPrefab(type);
            RectTransform rect = obj.GetComponent<RectTransform>();
            rect.sizeDelta = new Vector2(80, 80);
            itemObject = obj;
        }
    }
    
    private List<CraftingDisplay> displayedCraftingOptions = new List<CraftingDisplay>();
    private Dictionary<ItemPrefabType, List<CraftingDisplay>> craftingRecipes = 
        new Dictionary<ItemPrefabType, List<CraftingDisplay>>();

    // Use this for initialization
    void Start ()
    {
        gm = FindObjectOfType<GameManager>();
        gm.SetCraftingMenu(this);
        Debug.Assert(optionPrefab != null, "CraftingScript missing OptionPrefab");
        Debug.Assert(craftingOptionsPanel != null, "CraftingScript missing craftingOptionsPanel");
        Debug.Assert(requirementsPanel != null, "CraftingScript missing requirementsPanel");
        Debug.Assert(requirementPrefab != null, "CraftingScript missing requirementPrefab");
        Debug.Assert(bloodFramePrefab != null, "CraftingScript missing bloodFramePrefab");

        bloodFrame = Instantiate(bloodFramePrefab) as GameObject;

        InitCraftingOptions();
    }

    private void InitCraftingOptions()
    {
        craftingRecipes.Add(ItemPrefabType.Torch,
            new List<CraftingDisplay>() {
                new CraftingDisplay(ItemPrefabType.Coal),
                new CraftingDisplay(ItemPrefabType.Branch) });

        craftingRecipes.Add(ItemPrefabType.Pickaxe,
            new List<CraftingDisplay>() {
                new CraftingDisplay(ItemPrefabType.Rock),
                new CraftingDisplay(ItemPrefabType.Branch),
                new CraftingDisplay(ItemPrefabType.Branch) });

        displayedCraftingOptions.Add( new CraftingDisplay(ItemPrefabType.Torch));
        displayedCraftingOptions.Add( new CraftingDisplay(ItemPrefabType.Pickaxe));

        craftMenuOpen = false;
        HideCraftingMenu();
    }

    private void DisplayCraftingRequirementsFor(ItemPrefabType itemType)
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            ScrollUp();
        if (Input.GetKeyDown(KeyCode.DownArrow))
            ScrollDown();
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
            AttemptCraft();
        if (Input.GetKeyDown(KeyCode.Y)) // pick up item
        {
            CraftingDisplay item = displayedCraftingOptions[selectedIndex];
            List<CraftingDisplay> displayedRequirements = craftingRecipes[item.type];
            CalculateRequirementsMet(displayedRequirements);
        }
    }

    private Dictionary<CraftingDisplay, bool> m_equirementsToSatisfied = new Dictionary<CraftingDisplay, bool>();
    private bool m_allReqsSatified = false;

    private void CalculateRequirementsMet(List<CraftingDisplay> requirements )
    {
        Dictionary<ItemPrefabType, int> reqsToQuantity = new Dictionary<ItemPrefabType, int>();
        foreach (CraftingDisplay req in requirements)
        {
            if (reqsToQuantity.ContainsKey(req.type))
                reqsToQuantity[req.type]++;
            else
                reqsToQuantity.Add(req.type, 1);
        }

        Dictionary<ItemPrefabType, int> itemsToQuantity = new Dictionary<ItemPrefabType, int>();
        foreach (Item item in gm.inventory.m_hotBarItems)
        {
            if (reqsToQuantity.ContainsKey(item.m_type))
            {
                if (itemsToQuantity.ContainsKey(item.m_type))
                    itemsToQuantity[item.m_type]++;
                else
                    itemsToQuantity.Add(item.m_type, 1);
            }
        }

        m_equirementsToSatisfied.Clear();
        m_allReqsSatified = true;
        foreach (CraftingDisplay req in requirements)
        {
            m_equirementsToSatisfied.Add(req, false);
            if (itemsToQuantity.ContainsKey(req.type) && itemsToQuantity[req.type] > 0)
            {
                itemsToQuantity[req.type]--;
                m_equirementsToSatisfied[req] = true;
            }
            else
            {
                m_allReqsSatified = false;
            }

            req.itemObject.GetComponent<Image>().color = m_equirementsToSatisfied[req] ? White : Red;
        }
    }

    private void AttemptCraft()
    {
        CraftingDisplay selectedItem = displayedCraftingOptions[selectedIndex];
        List<CraftingDisplay> requirements = craftingRecipes[selectedItem.type];

        if (m_allReqsSatified)
        {
            foreach (CraftingDisplay item in requirements)
            {
                gm.inventory.RemoveItem(item.type);
            }
            gm.inventory.AddHotbarItem(ItemFactory.CreateItem(selectedItem.type));
        }
    }

    public void ScrollUp()
    {
        if (selectedIndex > 0)
        {
            DeselectCraftingOption(selectedIndex);
            selectedIndex = Mathf.Max(selectedIndex - 1, 0);
            SelectCraftingOption(selectedIndex);
        }
    }
    public void ScrollDown()
    {
        if (selectedIndex < displayedCraftingOptions.Count - 1)
        {
            DeselectCraftingOption(selectedIndex);
            selectedIndex = Mathf.Min(selectedIndex + 1, displayedCraftingOptions.Count - 1);
            SelectCraftingOption(selectedIndex);
        }
    }

    public void HideCraftingMenu()
    {
        craftMenuOpen = false;
        this.gameObject.SetActive(false);
    }

    public void ShowCraftingMenu()
    {
        craftMenuOpen = true;
        this.gameObject.SetActive(true);
        foreach ( CraftingDisplay item in displayedCraftingOptions)
            item.itemObject.transform.SetParent(craftingOptionsPanel.transform);
        SelectCraftingOption(selectedIndex);
    }

    private void DeselectCraftingOption(int index)
    {
        ItemPrefabType displayedOptionType = displayedCraftingOptions[index].type;
        List<CraftingDisplay> displayedRequirements = craftingRecipes[displayedOptionType];

        // unparent the transforms
        foreach (CraftingDisplay requirement in displayedRequirements)
        {
            requirement.itemObject.transform.SetParent(null);
        }
    }


    private Color32 White = new Color32(255, 255, 255, 100);
    private Color32 Red = new Color32(255, 0, 0, 255);
    private void SelectCraftingOption(int index)
    {
        CraftingDisplay item = displayedCraftingOptions[index];

        RectTransform itemTransform = item.itemObject.GetComponent<RectTransform>();
        Debug.Assert(itemTransform != null);
        float itemHeight = itemTransform.sizeDelta.y;
        requirementsPanel.GetComponent<HorizontalLayoutGroup>().padding.top = 
            this.minPaddingTop + (int)(itemHeight * index);

        bloodFrame.transform.SetParent(item.itemObject.transform);
        bloodFrame.transform.position = new Vector3(item.itemObject.transform.position.x,
                                                    item.itemObject.transform.position.y,
                                                    0);

        List<CraftingDisplay> displayedRequirements = craftingRecipes[item.type];

        CalculateRequirementsMet(displayedRequirements);

        // parent the transforms
        foreach (CraftingDisplay requirement in displayedRequirements)
            requirement.itemObject.transform.SetParent(requirementsPanel.transform);
    }    
}

