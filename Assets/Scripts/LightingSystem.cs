﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingSystem : MonoBehaviour
{
    private GameObject hero;
    private GameManager gameManager;

    public void setHero(GameObject _hero)
    {
        hero = _hero;
    }
    public void setGameManager(GameObject _gameManager)
    {
        gameManager = _gameManager.GetComponent<GameManager>();

        m_darknessMask = Instantiate(gameManager.m_darknessMaskPrefab) as GameObject;
    }

    public void ApplyLights(List<Torch> torches)
    {
        //m_darknessMask.SetActive(false);
        //return;
        if (torches == null)
            return;

        // initialize all torches to default opacity
        InitializeDarknessMask();

        var heroTransform = hero.GetComponent<RectTransform>();

        // TODO - currently hacked/assuming we're moving the one torch on the player, with the player.
        //        if we ever want more than one torch to move, we'll need to revisit this moarsmarter

        foreach (Torch torch in torches)
            if (torch.movesWithHero)
            {
                torch.x = heroTransform.position.x;
                torch.y = heroTransform.position.y;
                break;
            }

        foreach (Torch torch in torches)
        {
            UpdateMask(torch);
        }


        m_darknessMaskTexture.Apply();
        m_darknessMask.GetComponent<Renderer>().material.mainTexture = m_darknessMaskTexture;

        // TODO - do we need to recreate the sprite every update?  can we do this once and just overright the texture>?
        m_darknessMask.GetComponent<SpriteRenderer>().sprite =
            Sprite.Create(m_darknessMaskTexture, new Rect(new Vector2(0, 0),
                new Vector2(m_darknessMaskTexture.width, m_darknessMaskTexture.height)),
                new Vector2(0.5f, 0.5f), 100);
    }
    private static float darknessMaskZ = -9f;


    public void UpdateDarknessMaskPosition(float x, float y)
    {
        m_darknessMask.transform.position =
            new Vector3(x, y, darknessMaskZ);
    }


    bool MaskOpacityEquals(float alpha1, float alpha2)
    {
        if (Mathf.Abs(alpha1 - alpha2) <= 0.01)
            return true;
        return false;
    }

    private void InitializeDarknessMask()
    {
        for (int i = 0; i < m_textureSize; ++i)
            for (int j = 0; j < m_textureSize; ++j)
                m_darknessMaskTexture.SetPixel(i, j, new Color(0.0f, 0.0f, 0.0f, m_deepDarkOpacity));
    }


    public float m_defaultOpacity;
    public float m_deepDarkOpacity;
    public int m_textureSize;
    private int m_halfTexSize;
    private Texture2D m_darknessMaskTexture;
    private GameObject m_darknessMask;

    public class Torch
    {
        public float x, y;
        public bool movesWithHero;
        public Torch(float _x, float _y, bool _movesWithHero)
        {
            x = _x; y = _y; movesWithHero = _movesWithHero;
        }
    };

    private void UpdateMask(Torch torch)
    {
        //m_darknessMask.SetActive(false);
        //return;
		
        float opacity = m_defaultOpacity;
        int originalRadius = 40;
        int originX = 0, originY = 0, radius = 0;
        bool shouldDrawLight = false;

        // math out where in the darknessmask texture, we should render the torch
        {
            Vector3 torchVec = new Vector3(torch.x, torch.y, 0);
            Vector3 cameraVec = gameManager.m_camera.transform.position;
            Plane darknessMaskPlane = new Plane(Vector3.back, Vector3.forward * darknessMaskZ);
            Vector3 torchToCameraDir = new Vector3(torchVec.x - cameraVec.x, torchVec.y - cameraVec.y, torchVec.z - cameraVec.z);

            float distance;
            bool isParallel = darknessMaskPlane.Raycast(new Ray(torchVec, torchToCameraDir), out distance);

            Ray myRay = new Ray(torchVec, torchToCameraDir);
            float hacky7percentMoreDistanceForSomeReason = 1.07f;
            Vector3 positionOnMaskPlane = myRay.GetPoint(distance  * hacky7percentMoreDistanceForSomeReason);

            RectTransform trans = m_darknessMask.GetComponent<RectTransform>();
            Vector3[] corners = new Vector3[4];
            trans.GetWorldCorners(corners);
            Vector3 bottomLeft = corners[0];
            Vector3 topRight = corners[2];
            float worldSpaceWidth = topRight.x - bottomLeft.x;
            float ratio = (m_textureSize / worldSpaceWidth) ;
            float worldSpaceOffsetX = bottomLeft.x;
            float worldSpaceOffsetY = bottomLeft.y;
            float cameraPositionNoOffset = m_darknessMask.transform.position.x - worldSpaceOffsetX;
            float cameraOriginPixelSpace = cameraPositionNoOffset * ratio;

            if (!(positionOnMaskPlane.x < bottomLeft.x ||
                  positionOnMaskPlane.x > topRight.x ||
                  positionOnMaskPlane.y < bottomLeft.y ||
                  positionOnMaskPlane.y > topRight.y))
            {
                float torchPositionNoOffsetX = positionOnMaskPlane.x - worldSpaceOffsetX;
                float torchOriginPixelSpaceX = torchPositionNoOffsetX * ratio;
                float torchPositionNoOffsetY = positionOnMaskPlane.y - worldSpaceOffsetY;
                float torchOriginPixelSpaceY = torchPositionNoOffsetY * ratio;
                originX = (int)torchOriginPixelSpaceX;
                originY = (int)torchOriginPixelSpaceY;
                shouldDrawLight = true;
            }

        }

        if (shouldDrawLight)
        {
            radius = originalRadius; // TODO - based off of torch size
            float opacityDelta = 0.03f;
            int threshold = radius / 4;

#if UNITY_EDITOR
            //drawcircle(m_darknessMaskTexture, originX, originY, radius + 1, new Color(1.0f, 0.0f, 0.0f, 1.0f), false);
#endif

            while (radius > 1)
            {
                if (radius % threshold == 0)
                    opacityDelta *= 2;
                radius--;
                opacity -= opacityDelta;
                if (opacity < 0)
                    opacity = 0;
                drawcircle(m_darknessMaskTexture, originX, originY, radius, new Color(0.0f, 0.0f, 0.0f, opacity), true);
            }

            float halfRad = originalRadius * 0.75f;
            int cleanupStartX = originX - (int)halfRad;
            int cleanupStartY = originY - (int)halfRad;
            int cleanupEndX = originX + (int)halfRad;
            int cleanupEndY = originY + (int)halfRad;

            for (int i = cleanupStartX; i < cleanupEndX; ++i)
                for (int j = cleanupStartY; j < cleanupEndY; ++j)
                    if (MaskOpacityEquals(m_darknessMaskTexture.GetPixel(i, j).a, m_deepDarkOpacity))
                        m_darknessMaskTexture.SetPixel(i, j, m_darknessMaskTexture.GetPixel(i - 1, j));
        }
    }

    private void computePixel( Texture2D texture, int x, int y, Color newColor, bool multiply)
    {
        if (multiply)
        {
            Color color = texture.GetPixel(x, y);
            color.a *= newColor.a;
            texture.SetPixel(x, y, color);
        }
        else
            texture.SetPixel(x, y, newColor);
    }

    private void drawcircle(Texture2D texture, int x0, int y0, int radius, Color color, bool multiply)
    {
        int x = radius - 1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);

        while (x >= y)
        {
            computePixel(texture, x0 + x, y0 + y, color, multiply);
            computePixel(texture, x0 + y, y0 + x, color, multiply);
            computePixel(texture, x0 - y, y0 + x, color, multiply);
            computePixel(texture, x0 - x, y0 + y, color, multiply);
            computePixel(texture, x0 - x, y0 - y, color, multiply);
            computePixel(texture, x0 - y, y0 - x, color, multiply);
            computePixel(texture, x0 + y, y0 - x, color, multiply);
            computePixel(texture, x0 + x, y0 - y, color, multiply);

            if (err <= 0)
            {
                y++;
                err += dy;
                dy += 2;
            }

            if (err > 0)
            {
                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }
    }

    void Update()
    {
    }
    void Start()
    {
        m_textureSize = 300;
        m_darknessMaskTexture = new Texture2D(m_textureSize, m_textureSize, TextureFormat.ARGB32, false);
        m_halfTexSize = m_textureSize / 2;
    }
}

