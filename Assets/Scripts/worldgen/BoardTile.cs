﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BoardTile
{
    public TileType m_tileType;
    public GameObject m_boardTile;
    public Room m_room; // room i'm a part of; may be null if part of pathway.  not currently used.
    public enum BlightLevel { NotBlighted, Blight1, Blight2, Blight3, Blight4 };
    public BlightLevel m_blightLevel;
    public int m_x, m_y;

    public BoardTile(int x, int y, TileType tileType, GameObject boardTile, Room room)
    {
        m_x = x; m_y = y;
        m_tileType = tileType;
        m_boardTile = boardTile;
        m_room = room;
        m_blightLevel = BlightLevel.NotBlighted;
    }

    public void Cleanup()
    {
        m_boardTile = null;
    }

    public enum TileType { Wall, Floor };
    public bool IsTraversible()
    {
        return m_tileType == TileType.Floor;
    }

    public void BecomeBlightSource()
    {
        m_blightLevel = BlightLevel.Blight4;
        SetSprite();
    }

    private void SetSprite()
    {
        SetSpriteForTileType(m_boardTile.GetComponent<SpriteRenderer>());
    }

    public void SetSpriteForTileType(SpriteRenderer spriteRenderer)
    {
        Sprite sprite = null;
        switch (m_blightLevel)
        {
            case BlightLevel.NotBlighted:
                sprite = BoardManager.s_bmInstance.m_stoneFloorDefault; break;
            case BlightLevel.Blight1:
                {
                    int rand = Random.Range(0, 3);
                    sprite = rand == 0 ? 
                        BoardManager.s_bmInstance.m_stoneFloorBlight1a :
                        BoardManager.s_bmInstance.m_stoneFloorBlight1b;
                }
                break;
            case BlightLevel.Blight2:
                sprite = BoardManager.s_bmInstance.m_stoneFloorBlight2a; break;
            case BlightLevel.Blight3:
                sprite = BoardManager.s_bmInstance.m_stoneFloorBlight3a;
                //spriteRenderer.color = new Color(0.8f, 0.8f, 0.8f); 
                break;
            case BlightLevel.Blight4:
                sprite = BoardManager.s_bmInstance.m_stoneFloorBlight3aDecor;
                //spriteRenderer.color = new Color(0.5f, 0.5f, 0.5f); 
                break;
            default:
                Debug.Assert(false, "shouldn't get here");
                throw new System.Exception("unknown blight level");
        }
        spriteRenderer.sprite = sprite;
    }

    public BoardTile CheckBlightLevel(int spreadThreshold, int spreadRangeMax, int blightLevelThreshold, int blightLevelRangeMax)
    {
        if (m_blightLevel == BlightLevel.NotBlighted)
        {
            if (Random.Range(0, spreadRangeMax) < spreadThreshold)
            {
                m_blightLevel = m_blightLevel + 1;
                SetSprite();
                return this;
            }
        }
        else
        {
            if (Random.Range(0, blightLevelRangeMax) < blightLevelThreshold)
            {
                m_blightLevel = m_blightLevel + 1;
                if (m_blightLevel > BlightLevel.Blight3)
                    m_blightLevel = BlightLevel.Blight3;
                SetSprite();
            }
        }
        return null;
    }

    // returns newly blighted tiles
    public List<BoardTile> RadialSpread(int spreadThreshold, int spreadRangeMax, int blightLevelThreshold, int blightLevelRangeMax)
    {
        /*  'O' has a chance to spread to adjacent flooring ('X's)
         * 
         *    X X X
         *    X O X
         *    X X X
         * 
         */
        List<BoardTile> newlyBlightedTiles = new List<BoardTile>();
        CheckBlightLevelAndTrackNewBlights(newlyBlightedTiles, m_x - 1, m_y - 1, spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax);
        CheckBlightLevelAndTrackNewBlights(newlyBlightedTiles, m_x - 1, m_y, spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax);
        CheckBlightLevelAndTrackNewBlights(newlyBlightedTiles, m_x - 1, m_y + 1, spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax);
        CheckBlightLevelAndTrackNewBlights(newlyBlightedTiles, m_x, m_y - 1, spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax);
        CheckBlightLevelAndTrackNewBlights(newlyBlightedTiles, m_x, m_y + 1, spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax);
        CheckBlightLevelAndTrackNewBlights(newlyBlightedTiles, m_x + 1, m_y - 1, spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax);
        CheckBlightLevelAndTrackNewBlights(newlyBlightedTiles, m_x + 1, m_y, spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax);
        CheckBlightLevelAndTrackNewBlights(newlyBlightedTiles, m_x + 1, m_y + 1, spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax);
        return newlyBlightedTiles;
    }

    private void CheckBlightLevelAndTrackNewBlights(List<BoardTile> trackingNewBlights, int x, int y,
                                                    int spreadThreshold, int spreadRangeMax, int blightLevelThreshold, int blightLevelRangeMax)
    {
        BoardTile tileToCheck = null;
        if (BoardManager.s_bmInstance.IsGridTraversible(x, y, out tileToCheck))
            if (tileToCheck.CheckBlightLevel(spreadThreshold, spreadRangeMax, blightLevelThreshold, blightLevelRangeMax) != null)
                trackingNewBlights.Add(tileToCheck);
    }

    public void UpdateBlight(List<BoardTile> newSources, List<BoardTile> sourcesToRemove)
    {
        newSources.Clear();
        sourcesToRemove.Clear();

        switch (m_blightLevel)
        {
            case BlightLevel.Blight1:
                newSources.AddRange(RadialSpread(1, 4, 1, 10)); break;
            case BlightLevel.Blight2:
                newSources.AddRange(RadialSpread(1, 4, 1, 40)); break;
            case BlightLevel.Blight3:
                newSources.AddRange(RadialSpread(1, 4, 1, 80)); sourcesToRemove.Add(this); break;
            case BlightLevel.Blight4:
                newSources.AddRange(RadialSpread(1, 4, 1, 80)); sourcesToRemove.Add(this); break;
            // TODO: - we might want these to spread a bit further/faster, possibly create new level 4 blighted squares

            default: Debug.Assert(false, "shouldn't get here"); break;
        }
    }
};