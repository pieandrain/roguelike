﻿using System.Collections;
using System.Collections.Generic;

public class Blight
{
    public Blight(BoardManager _bm, int _x, int _y)
    {
        bm = _bm;
        boardX = _x;
        boardY = _y;
        sources = new List<BoardTile>();
        sources.Add(bm.grid[boardX][boardY]);
        bm.grid[boardX][boardY].BecomeBlightSource();
    }
    public void Update()
    {
        List<BoardTile> newSourceBlights = new List<BoardTile>();
        List<BoardTile> sourceBlightsToRemove = new List<BoardTile>();
        foreach (BoardTile blightedTile in sources)
        {
            blightedTile.UpdateBlight(newSourceBlights, sourceBlightsToRemove);
            if (newSourceBlights != null && newSourceBlights.Count > 0)
                newSourceBlights.AddRange(newSourceBlights);
        }

        sources.AddRange(newSourceBlights);

        foreach (BoardTile toRemove in sourceBlightsToRemove)
        {
            sources.Remove(toRemove);
        }

    }
    private BoardManager bm = null;
    private int boardX, boardY;
    private List<BoardTile> sources = null;
};
