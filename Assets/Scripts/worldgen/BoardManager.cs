﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour
{
    private const float blightPulseCheckTime = 2.0f;

    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject topWall;

    // x -> y -> BoardTile
    public Dictionary<int, Dictionary<int, BoardTile>> grid = new Dictionary<int, Dictionary<int, BoardTile>>();
    public GameManager gm;
    private GameObject m_bloodyFloorPrefab;
    public List<Room> m_rooms = new List<Room>(); // TODO: will be contained within a region.
    public Sprite m_stoneFloorDefault;
    public Sprite m_stoneFloorBloody;
    public Sprite m_stoneFloorMoss1;
    public Sprite m_stoneFloorMoss2;
    public Sprite m_stoneFloorBlight1a;
    public Sprite m_stoneFloorBlight1b;
    public Sprite m_stoneFloorBlight2a;
    public Sprite m_stoneFloorBlight3a;
    public Sprite m_stoneFloorBlight3aDecor;
    public GameObject craftItem;

    public static BoardManager s_bmInstance = null;

    void Start()
    {
        m_bloodyFloorPrefab = Resources.Load("Floor2BloodyPrefab") as GameObject;
        LoadFloorResources();
        s_bmInstance = this;

        // All of this is subject to change
        // for now, a 'room' will periodically spawn a 'blight'.  
        //  once a blight is spawned, that blight will slowly spread
        // TODO: implementation is pretty naive... it will be more informed when we flesh out what we want to do with blight/tile-updates/game-progression/pacing... etc.
        StartCoroutine(CheckForBlight());
    }

    public IEnumerator CheckForBlight()
    {
        while (true)
        {
            yield return new WaitForSeconds(blightPulseCheckTime);
            foreach (Room room in m_rooms)
            {
                if (!room.IsBlighted() && Random.Range(0, 10) < 100)
                    room.CreateBlight();
                else
                    room.UpdateBlight();

            }
        }
    }

    public void UpdateBoard()
    {
        foreach (Room room in m_rooms)
            room.Update();
    }

    public void LoadFloorResources()
    {
        m_stoneFloorDefault = Resources.Load("StoneFloor", typeof(Sprite)) as Sprite;
        m_stoneFloorBloody = Resources.Load("StoneFloor", typeof(Sprite)) as Sprite;  // TODO - need to reimport this piece of art
        m_stoneFloorMoss1 = Resources.Load("StoneFloor_mold_1", typeof(Sprite)) as Sprite;
        m_stoneFloorMoss2 = Resources.Load("StoneFloor_mold_2", typeof(Sprite)) as Sprite;
        m_stoneFloorBlight1a = Resources.Load("StoneFloor_blight_1a", typeof(Sprite)) as Sprite;
        m_stoneFloorBlight1b = Resources.Load("StoneFloor_blight_1b", typeof(Sprite)) as Sprite;
        m_stoneFloorBlight2a = Resources.Load("StoneFloor_blight_2a", typeof(Sprite)) as Sprite;
        m_stoneFloorBlight3a = Resources.Load("StoneFloor_blight_3a", typeof(Sprite)) as Sprite;
        m_stoneFloorBlight3aDecor = Resources.Load("StoneFloor_blight_3a_decor1", typeof(Sprite)) as Sprite;
    }

    public GameObject CreateFloorTile()
    {
        return Instantiate(floorTiles[0]) as GameObject;
    }


    public GameObject CreateWallTile()
    {
        return Instantiate(wallTiles[(int)Random.Range(0, wallTiles.Length)]) as GameObject;
    }

    private GameObject GetTileInstanceAt(int x, int y, GameObject inheritingParent)
    {
        GameObject floorInstance = Instantiate(floorTiles[0]) as GameObject;
        if(inheritingParent != null)
            floorInstance.transform.SetParent(inheritingParent.transform);

        RectTransform rect = floorInstance.AddComponent<RectTransform>();
        rect.position = new Vector3(x, y, 0);

        return floorInstance;
    }

    public void CreateDungeon()  
    {
        // defines the total size of this region
        int maxTilesPerSide = (int)(150 * 0.5f);
        int minX = -maxTilesPerSide;
        int minY = -maxTilesPerSide;
        int maxX = maxTilesPerSide;
        int maxY = maxTilesPerSide;

        // Generate rooms in this region

        // Starting Room TODO: we're assuming the hero is at 2,2
        CreateRoom(-2, -2, 12, 12, 0, 40);

        // boundaries for individual room size
        int minWidth = 8; int maxWidth = 20;
        int minHeight = 8; int maxHeight = 20;

        // keep the number of rooms even for pathway logic (21 maxRooms + 1 starting room == 22)
        int maxRooms = 21; // TODO: scale to size of available space
        int maxRoomAttempts = 5;
        for (int i = 0; i < maxRooms; ++i)
        {
            for (int j = 0; j < maxRoomAttempts; ++j)
            {
                int width = Random.Range(minWidth, maxWidth);
                int height = Random.Range(minHeight, maxHeight);
                int llx = Random.Range(minX, maxX);
                int lly = Random.Range(minY, maxY);

                // throw out this room if it intersects w/ an already existing room
                if (Room.SpaceExistsForRoom(grid, llx, lly, llx + width, lly + height))
                {
                    CreateRoom(llx, lly, width, height, (int)Random.Range(1,2), (int)Random.Range(1,4));
                    break;
                }
            }
        }

        // split up the rooms into 2 lists.  connect them up.
        List<Room> rooms1 = new List<Room>();
        List<Room> rooms2 = new List<Room>();
        bool toRooms1 = true;
        foreach (Room currentRoom in m_rooms)
        {
            if (toRooms1)
                rooms1.Add(currentRoom);
            else
                rooms2.Add(currentRoom);
            toRooms1 = !toRooms1;
        }

        int count = rooms1.Count;
        for (int i = 0; i < count; ++i)
            CreatePathway(rooms1[i], rooms2[i]);
    }
    public bool IsGridTraversible(int x, int y, out BoardTile tile)
    {
        tile = null;
        if (!grid.ContainsKey(x))
            return false;
        if (!grid[x].ContainsKey(y))
            return false;
        tile = grid[x][y];
        return tile.IsTraversible();
    }

    private void CreateTile(int x, int y, GameObject pathway)
    {
        if (!grid.ContainsKey(x))
        {
            grid.Add(x, new Dictionary<int, BoardTile>());
            GameObject floorInstance = GetTileInstanceAt(x, y, pathway);
            grid[x].Add(y, new BoardTile(x, y, BoardTile.TileType.Floor, floorInstance, null)); // TODO: not sure if these should be contained in a Pathway class or something.  probably.
        }
        else
        {
            if (!grid[x].ContainsKey(y))
            {
                GameObject floorInstance = GetTileInstanceAt(x, y, pathway);
                grid[x].Add(y, new BoardTile(x, y, BoardTile.TileType.Floor, floorInstance, null)); // TODO: not sure if these should be contained in a Pathway class or something.  probably.
            }
            else
            {
                if (grid[x][y].m_tileType == BoardTile.TileType.Wall)
                {
                    GameObject floorInstance = GetTileInstanceAt(x, y, pathway);

                    // make sure to use the room this wall was a part of
                    BoardTile newBoardTile = new BoardTile(x, y, BoardTile.TileType.Floor, floorInstance, grid[x][y].m_room);
                    grid[x][y].Cleanup();
                    grid[x][y] = newBoardTile;
                }
            }
        }
    }

    public void DeleteTiles()
    {
        foreach (var row in grid.Values)
        {
            foreach (BoardTile tile in row.Values)
            {
                Destroy(tile.m_boardTile);
                tile.Cleanup();
            }
        }
    }

    private void CreatePathSegment(int x, int y, GameObject pathway)
    {
        CreateTile(x, y     , pathway);
        CreateTile(x, y + 1 , pathway);
        CreateTile(x+1, y, pathway);
        CreateTile(x+1, y+1, pathway);
    }

    private static int pathCounter = 0;
    private void CreatePathway(Room roomA, Room roomB)
    {
        ++pathCounter;
        GameObject pathway = new GameObject("Path " + pathCounter.ToString());

        Vector2 currentPosition = new Vector2( roomA.centerTile.x, roomA.centerTile.y);
        Vector2 endingPosition = new Vector2( roomB.centerTile.x, roomB.centerTile.y);

        int infiniteLoopCounter = 0;
        int loopMax = 500;
        bool done = false;
        while(!done)
        {
            Vector2 targetDirection = endingPosition - currentPosition;
            bool xIsDominantDirection = Mathf.Abs(targetDirection.x) > Mathf.Abs(targetDirection.y);
            if (xIsDominantDirection)
                currentPosition.x += targetDirection.x > 0 ? 1 : -1;
            else
                currentPosition.y += targetDirection.y > 0 ? 1 : -1;

            int x = (int)currentPosition.x;
            int y = (int)currentPosition.y;

            CreatePathSegment(x, y, pathway);

            if ((x == (int)endingPosition.x || x == (int)(endingPosition.x + 0.5f)) &&
                (y == (int)endingPosition.y || y == (int)(endingPosition.y + 0.5f)))
            {
                roomA.connectedRooms.Add(roomB);
                roomB.connectedRooms.Add(roomA);
                done = true;
            }
            if (++infiniteLoopCounter > loopMax)
            {
                string debugMsg = "Infinite Loop detected! Set a break point here! --" +
                    " x:" + x.ToString() + " y:" + y.ToString() +
                    " endX:" + endingPosition.x.ToString() +
                    " endY:" + endingPosition.y.ToString();
                Debug.Assert(false, debugMsg);
                done = true;
            }
        }
    }

    private void CreateRoom(int llx, int lly, int width, int height, int numBadGuysToSpawn, int numItemsToSpawn)
    {
        Debug.Assert(gm != null);
        int xStart = llx; int yStart = lly;
        int xEnd = llx + width; int yEnd = lly + height;
        Room newRoom = new Room(this, xStart, yStart, xEnd, yEnd);
        m_rooms.Add(newRoom);
        newRoom.Create();

        for (int i=0; i< numBadGuysToSpawn; ++i)
        {
            GameObject go = Instantiate(gm.m_badguyPrefab) as GameObject;
            go.transform.position =
                new Vector3(Random.Range(xStart + 1, xEnd - 1), // account for walls
                            Random.Range(yStart + 1, yEnd - 1), 0);
            gm.m_enemies.Add(go);
            BadGuyMelee bgm = go.GetComponent<BadGuyMelee>();
            bgm.hero = gm.m_hero;
            bgm.gm = gm;
            newRoom.AddObject(go);

            SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
            sr.enabled = true;
        }

        for (int i = 0; i < numItemsToSpawn; ++i)
        {
            int val = Random.Range(0, 3);
            switch (val)
            {
                case 0:
                    {
                        Item item = CreateRandomFood();
                        newRoom.AddObject(item.m_item);
                        gm.PlaceItemInWorld(Random.Range(xStart + 1, xEnd - 1), // account for walls
                                            Random.Range(yStart + 1, yEnd - 1),
                                            item );
                    }
                    break;
                case 1:
                case 2:
                    {
                        Item item = CreateRandomCrafting();
                        newRoom.AddObject(item.m_item);
                        gm.PlaceItemInWorld(Random.Range(xStart + 1, xEnd - 1), // account for walls
                                            Random.Range(yStart + 1, yEnd - 1),
                                            item);
                    }
                    break;
                    
            }
        }
    }

    private Item CreateRandomFood()
    {
        Item item = null;
        int val = Random.Range(0, 4);
        switch (val)
        {
            case 0: item = ItemFactory.CreateItem(ItemPrefabType.Crab); break;
            case 1: item = ItemFactory.CreateItem(ItemPrefabType.Bread); break;
            case 2: item = ItemFactory.CreateItem(ItemPrefabType.Melon); break;
            case 3: item = ItemFactory.CreateItem(ItemPrefabType.Water); break;
            default: Debug.Assert(false, "shouldn't happen!"); break;
        }

        return item;
    }

    private Item CreateRandomCrafting()
    {
        Item item = null;
        int val = Random.Range(0, 7);
        switch (val)
        {
            case 0: item = ItemFactory.CreateItem(ItemPrefabType.Branch); break;
            case 1: item = ItemFactory.CreateItem(ItemPrefabType.Cloth); break;
            case 2: item = ItemFactory.CreateItem(ItemPrefabType.Coal); break;
            case 3: item = ItemFactory.CreateItem(ItemPrefabType.Mud); break;
            case 4: item = ItemFactory.CreateItem(ItemPrefabType.Rock); break;
            case 5: item = ItemFactory.CreateItem(ItemPrefabType.Root); break;
            case 6: item = ItemFactory.CreateItem(ItemPrefabType.Salt); break;
            default: Debug.Assert(false, "shouldn't happen!"); break;
        }

        return item;
    }
    
}
