﻿using System.Collections.Generic;
using UnityEngine;

public class Room
{
    public Room(BoardManager _bm, int _llx, int _lly, int _urx, int _ury)
    {
        bm = _bm;
        llx = _llx; lly = _lly; urx = _urx; ury = _ury;
        centerTile = new Vector2(llx + (urx - llx) * 0.5f, lly + (ury - lly) * 0.5f);
        connectedRooms = new List<Room>();
        ++roomCounter;
        parent = new GameObject("Room " + roomCounter.ToString());
        wallTilesParent = new GameObject("Wall Tiles");
        wallTilesParent.transform.SetParent(parent.transform);
        floorTilesParent = new GameObject("Floor Tiles");
        floorTilesParent.transform.SetParent(parent.transform);
    }

    public List<Room> connectedRooms; // rooms to which this room is connected via some pathway
    public BoardManager bm;
    private GameObject parent;
    private GameObject wallTilesParent;
    private GameObject floorTilesParent;

    // all cells in the grid within this (inclusive) bounding box are part of this room
    public int llx;
    public int lly;
    public int urx;
    public int ury;
    public Vector2 centerTile;
    private static int roomCounter = 0;
    public Blight mBlight = null;
    public bool IsBlighted()
    {
        return mBlight != null;
    }
    public void CreateBlight()
    {
        mBlight = new Blight(bm, Random.Range(llx + 1, urx - 1), Random.Range(lly + 1, ury - 1));
    }

    public void Create()
    {
        for (int x = llx; x <= urx; ++x)
        {
            for (int y = lly; y <= ury; ++y)
            {
                bool isWall = x == llx || y == lly || x == urx || y == ury;
                
                GameObject instance = isWall ? bm.CreateWallTile() : bm.CreateFloorTile();

                instance.transform.SetParent(isWall ? wallTilesParent.transform : floorTilesParent.transform);

                RectTransform floorRect = instance.AddComponent<RectTransform>();
                floorRect.position = new Vector3(x, y, 0);

                if (!bm.grid.ContainsKey(x))
                    bm.grid.Add(x, new Dictionary<int, BoardTile>());
                if (!bm.grid[x].ContainsKey(y))
                    bm.grid[x].Add(y, new BoardTile(x, y,
                        isWall ? BoardTile.TileType.Wall : BoardTile.TileType.Floor, instance, this));
            }
        }
    }


    public void Update()
    {
        // TODO - handle non-blight related updates/processing here
    }
    public void UpdateBlight()
    {
        Debug.Assert(mBlight != null);
        mBlight.Update();
    }

    public void AddObject(GameObject obj)
    {
        obj.transform.SetParent(parent.transform);
    }

    // represents a single X by Y open/traversable area.  Rooms are interconnected by hallways.
    // Many rooms make up a region.
    // Rooms will be part of a 'biome' that control items spawned there, persistant effects (blight) etc.

    public static bool SpaceExistsForRoom(
        Dictionary<int, Dictionary<int, BoardTile>> grid, int llx, int lly, int urx, int ury)
    {
        for (int i = llx; i <= urx; ++i)
        {
            for (int j = lly; j <= ury; ++j)
            {
                if (!grid.ContainsKey(i))
                    continue;

                if (grid[i].ContainsKey(j))
                    return false;
            }
        }
        return true;
    }
};