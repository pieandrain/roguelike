﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//TODO add additional stats to itemStatEffectDic so that an item can affect more than one stat
//TODO AddStatBarsToScene handle floats for half bars of stats and add half images also
//TODO updateStats needs to handle floats and half bars

public class StatScript : MonoBehaviour {

    public static Canvas m_canvas;

    static float s_maxHealth = 10;
    static float s_maxFood = 10;
    static float s_maxWater = 10;
    public static GameObject s_healthPanel;//holds health bars
    public static GameObject s_waterPanel;
    public static GameObject s_foodPanel;
    public static GameObject healthImage;//health image display for stat menu
    public static GameObject waterImage;
    public static GameObject foodImage;
    public static Image[] images;
    string imageName = "image";
    static Dictionary<ItemPrefabType, Dictionary<string, float>> itemStatEffectDic;
    public static Dictionary<ItemPrefabType, float> statPointDic;
    private IEnumerator coroutine;
    

    private void Start()
    {
        //coroutine = DepleteStats();
    }

    public void InitStats()
    {
        //m_canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        //InitStatsDic();
        //DisplayStatMenu();
    }

    public void InitStatsDic()
    {
        //itemStatEffectDic = new Dictionary<ItemType, Dictionary<string, float>>();
        //Dictionary<string, float> innerDic = new Dictionary<string, float>();
        //innerDic.Add("crabPrefab", 3);
        //itemStatEffectDic.Add(ItemType.Food, innerDic);

        //Dictionary<string, float> inner = new Dictionary<string, float>();
        //inner.Add("canteenPrefab", 3);
        //itemStatEffectDic.Add(ItemType.Water, inner);

        //statPointDic = new Dictionary<ItemType, float>();
        //statPointDic.Add(ItemType.Food, 6);
        //statPointDic.Add(ItemType.Water, 3);
        //statPointDic.Add(ItemType.Health, 6);
    }


    static void CreateStatPanels()
    {
        ////Health
        //healthImage = new GameObject();
        //var rect = healthImage.AddComponent<RectTransform>();
        //healthImage.transform.SetParent(m_canvas.transform);
        //rect.position = new Vector3(50, 585, 0);
        //rect.name = "healthImage";
        //rect.sizeDelta = new Vector2(25, 25);
        //SpriteRenderer renderer = healthImage.AddComponent<SpriteRenderer>();
        //Image render = healthImage.AddComponent<Image>();
        //healthImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("heart");

        ////health bar panel
        //s_healthPanel = new GameObject();
        //var r = s_healthPanel.AddComponent<RectTransform>();
        //s_healthPanel.transform.SetParent(m_canvas.transform);
        //r.position = new Vector3(90, 585, 0);
        //r.name = "healthPanel";
        //r.sizeDelta = new Vector2(100, 20);

        ////Water
        //waterImage = new GameObject();
        //var rect1 = waterImage.AddComponent<RectTransform>();
        //waterImage.transform.SetParent(m_canvas.transform);
        //rect1.position = new Vector3(50, 555, 0);
        //rect1.name = "waterImage";
        //rect1.sizeDelta = new Vector2(25, 25);
        //SpriteRenderer renderer1 = waterImage.AddComponent<SpriteRenderer>();
        //Image render1 = waterImage.AddComponent<Image>();
        //waterImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("water");

        ////water bar panel
        //s_waterPanel = new GameObject();
        //var r1 = s_waterPanel.AddComponent<RectTransform>();
        //s_waterPanel.transform.SetParent(m_canvas.transform);
        //r1.position = new Vector3(90, 555, 0);
        //r1.name = "waterPanel";
        //r1.sizeDelta = new Vector2(100, 20);

        ////Food
        //foodImage = new GameObject();
        //var rect2 = foodImage.AddComponent<RectTransform>();
        //foodImage.transform.SetParent(m_canvas.transform);
        //rect2.position = new Vector3(50, 520, 0);
        //rect2.name = "foodImage";
        //rect2.sizeDelta = new Vector2(25, 25);
        //SpriteRenderer renderer2 = foodImage.AddComponent<SpriteRenderer>();
        //Image render2 = foodImage.AddComponent<Image>();
        //foodImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("bread");

        ////food bar panel
        //s_foodPanel = new GameObject();
        //var r2 = s_foodPanel.AddComponent<RectTransform>();
        //s_foodPanel.transform.SetParent(m_canvas.transform);
        //r2.position = new Vector3(90, 520, 0);
        //r2.name = "foodPanel";
        //r2.sizeDelta = new Vector2(100, 20);
    }

    public void DisplayStatMenu()
    {
        //CreateStatPanels();
        //CreateStatBars();
    }


    // Update is called once per frame
    void Update() {

    }

    public static void CreateStatBars()
    {
        //int xPos = -20;
        //int yPos = 1;
        //String health = "healthBar";
        //String water = "water";
        //String food = "food";

        //String healthImg = "red";
        //String waterImg = "blue";
        //String foodImg = "yellow";

        //AddStatBarsToScene(xPos, yPos, s_healthPanel.transform, health, healthImg, statPointDic[ItemType.Health]);
        //AddStatBarsToScene(xPos, yPos, s_waterPanel.transform, water, waterImg, statPointDic[ItemType.Water]);
        //AddStatBarsToScene(xPos, yPos, s_foodPanel.transform, food, foodImg, statPointDic[ItemType.Food]);
    }

    public static void DestroyStatBars()
    {
        //foreach (Transform child in s_foodPanel.transform)
        //{
        //    GameObject.Destroy(child.gameObject);
        //}

        //foreach (Transform child in s_healthPanel.transform)
        //{
        //    GameObject.Destroy(child.gameObject);
        //}

        //foreach (Transform child in s_waterPanel.transform)
        //{
        //    GameObject.Destroy(child.gameObject);
        //}
    }

    public static void AddStatBarsToScene(int xPos, int yPos, Transform objTrans, String name, String img, float numBars)
    {
        
        //for (int i = 0; i < numBars; ++i)
        //{
        //    GameObject bars = new GameObject();
        //    bars.transform.name = name;
        //    bars.transform.SetParent(objTrans);
        //    bars.AddComponent<RectTransform>();

        //    RectTransform myRect = bars.GetComponent<RectTransform>();
        //    myRect.sizeDelta = new Vector2(3, 12);
        //    myRect.localPosition = new Vector3(xPos, yPos, 0);   
        
        //    bars.AddComponent<Image>();
        //    bars.GetComponent<Image>().sprite = Resources.Load<Sprite>(img);
        //    xPos += 5;
        //}
    }


    //when item is Used/equipped
    public static void UpdateStats()
    {
        DestroyStatBars();
        CreateStatBars();
    }

    //adjust for item Use/equip
    public static void ItemUpdateStat(string name, ItemPrefabType type)
    {
        //float effect = 0;

        //if (itemStatEffectDic.ContainsKey(type))
        //{
        //    if (itemStatEffectDic[type].ContainsKey(name))
        //    {
        //        effect = itemStatEffectDic[type][name];
        //    }
        //}
        //if (statPointDic.ContainsKey(type))
        //{
        //    statPointDic[type] += effect;
        //}
        //UpdateStats();

    }


    //used to decrease stats with time
    public static void DecreaseStat(ItemPrefabType type, float affect)
    {
        //if (statPointDic.ContainsKey(type))
        //{
        //    statPointDic[type] -= affect;
        //    UpdateStats();
        //}
    }

    //deplete stats over time as game progresses
    public IEnumerator DepleteStats()
    {

        //    while (StatScript.statPointDic[ItemType.Health] > 0)
        //    {
        //        yield return new WaitForSeconds(6.0f);

        //        if (StatScript.statPointDic[ItemType.Health] > 0)
        //        {
        //            StatScript.DecreaseStat(ItemType.Health, 1);
        //        }

        //        yield return new WaitForSeconds(3.0f);

        //        if (StatScript.statPointDic[ItemType.Food] > 0)
        //        {
        //            StatScript.DecreaseStat(ItemType.Food, 1);
        //        }

        //        yield return new WaitForSeconds(3.0f);

        //        if (StatScript.statPointDic[ItemType.Water] > 0)
        //        {
        //            StatScript.DecreaseStat(ItemType.Water, 1);
        //        } 
        //      }
        yield return null;
    }
}
