﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryScript : MonoBehaviour
{
    // TODO - items should live in an 'inventory' and be displayed in the 'hotbar' and in the 'crafting window'
    public List<Item> m_hotBarItems = new List<Item>();


    private int m_selectedItemsIndex = 0;
    private int m_hotBarItemSize = 35;
    private int m_hotBarItemPadding = 3;
    private int m_hotBarItemOrigin = -150;

    private Canvas m_myCanvas;
    private GameObject m_invPanel;
    private Color32 Black = new Color32(0, 0, 0, 100);
    private Color32 White = new Color32(255, 255, 255, 255);
    private enum Direction
    { Left, Right }

    void Start()
    {
        m_myCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        InitInventoryPanel();
    }
        
    private void CycleItemSelection(Direction direction)
    {
        if (direction == Direction.Left && m_selectedItemsIndex > 0)
        {
            Deselect(m_hotBarItems[m_selectedItemsIndex]);
            m_selectedItemsIndex = Mathf.Max(m_selectedItemsIndex - 1, 0);
            Select(m_hotBarItems[m_selectedItemsIndex]);
        }
        else if (direction == Direction.Right && m_selectedItemsIndex < m_hotBarItems.Count )
        {
            Deselect(m_hotBarItems[m_selectedItemsIndex]);
            m_selectedItemsIndex = Mathf.Min(m_selectedItemsIndex + 1, m_hotBarItems.Count -1);
            Select(m_hotBarItems[m_selectedItemsIndex]);
        }
    }

    private void Select(Item item)
    {
        item.m_item.GetComponent<Image>().color = White;
    }
    private void Deselect(Item item)
    {
        item.m_item.GetComponent<Image>().color = Black;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Assert(m_myCanvas != null);
     
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            CycleItemSelection(Direction.Right);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            CycleItemSelection(Direction.Left);
        }
    }

    private void InitInventoryPanel()
    {
        m_invPanel = new GameObject();
        m_invPanel.SetActive(true);
        var rect = m_invPanel.AddComponent<RectTransform>();
        m_invPanel.transform.SetParent(m_myCanvas.transform);
        rect.position = new Vector3(550, 55, 0);
        rect.name = "invPanel";
        rect.sizeDelta = new Vector2(400, 50);
        SpriteRenderer renderer = m_invPanel.AddComponent<SpriteRenderer>();
        Image render = m_invPanel.AddComponent<Image>();
        m_invPanel.GetComponent<Image>().sprite = Resources.Load<Sprite>("eggshell");
    }

    public Item GetSelectedHotbarItem()
    {
        if (m_hotBarItems.Count > 0)
            return m_hotBarItems[m_selectedItemsIndex];
        return null;
    }

    public Item GetAndRemoveSelectedHotbarItem()
    {
        Item item = GetSelectedHotbarItem();
        if(item != null)
            RemoveItem(item);
        return item;
    }

    public bool HasTorch()
    {
        foreach (Item item in m_hotBarItems)
            if (item.m_type == ItemPrefabType.Torch)
                return true;
        return false;
    }

    public Item GetAndRemoveItem(ItemPrefabType type)
    {
        foreach (Item item in m_hotBarItems)
            if (item.m_type == type)
            {
                Item ret = item;
                RemoveItem(item);
                return ret;
            }

        return null;
    }

    public void RemoveItem(Item item)
    {
        Debug.Assert(item != null);
        item.m_item.transform.SetParent(null);
        m_hotBarItems.Remove(item);
        UpdateHotBarImagery();
    }

    public void RemoveItem(ItemPrefabType itemOfType)
    {
        foreach (Item item in m_hotBarItems)
            if (item.m_type == itemOfType)
            {
                RemoveItem(item);
                break;
            }
    }

    public void InitHotbarList()
    {
        AddHotbarItem(ItemFactory.CreateItem(ItemPrefabType.Crab));
        AddHotbarItem(ItemFactory.CreateItem(ItemPrefabType.Branch));
        AddHotbarItem(ItemFactory.CreateItem(ItemPrefabType.Bread));
        AddHotbarItem(ItemFactory.CreateItem(ItemPrefabType.Torch));
        AddHotbarItem(ItemFactory.CreateItem(ItemPrefabType.Torch));
        AddHotbarItem(ItemFactory.CreateItem(ItemPrefabType.Torch));
    }

    public void AddHotbarItem(Item item)
    {
        m_hotBarItems.Add(item);

        item.m_item.transform.SetParent(m_invPanel.transform);
        UpdateHotBarImagery();
    }

    public void UpdateHotBarImagery()
    {
        if (m_hotBarItems.Count > 0)
        {
            int index = 0;
            foreach (Item item in m_hotBarItems)
            {
                Deselect(item);
                int x = m_hotBarItemOrigin + (m_hotBarItemSize * index) + (m_hotBarItemPadding * index);
                RectTransform myRect = item.m_item.GetComponent<RectTransform>();
                myRect.sizeDelta = new Vector2(m_hotBarItemSize, m_hotBarItemSize);
                myRect.localPosition = new Vector3(x, 0, 0);
                ++index;
            }

            m_selectedItemsIndex = Mathf.Min(m_selectedItemsIndex, m_hotBarItems.Count - 1);
            Select(m_hotBarItems[m_selectedItemsIndex]);
        }
    }

    public void UpdateInventory()
    {
        UpdateHotBarImagery();
    }
}
